import React, { PureComponent } from 'react'
import './generic-page.css'
import { withRouter } from 'react-router-dom'

class NotFound extends PureComponent {
  handleBackClick = () => {
    // this.props.history.goBack()
    this.props.history.go(-1)
  }

  handleForwardClick = () => {
    this.props.history.goForward()
    
  }

  handleRandomClick = () => {
    const random = Math.round(Math.random() * (10 - 1) + 1)
    this.props.history.push(`/videos?id=${random}`, {id: random})
    // Creo que el segundo parametro, el objeto con la llave id, no debe de ir, inclusive si lo eliminas sigue funcionando
  }

  render() {
    return (
      <div className='Page Not Found' >
        <h1>404</h1>
        <h3 className="SadFace">:(</h3>
        <h2>No hemos encontrado la página que buscabas</h2>
        <button 
          className='Button'
          onClick={this.handleBackClick}
          >
          Ir a la ruta anterior 👈
        </button>
        <button 
          className='Button'
          onClick={this.handleRandomClick}
          >
          Video random ☘
        </button>
        <button 
          className='Button'
          onClick={this.handleForwardClick}
          >
          Ir a la ruta siguiente 👉
        </button>
      </div>
    )
  }
}

export default withRouter(NotFound)