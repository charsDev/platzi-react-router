import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter} from 'react-router-dom'
import App from '../pages/containers/appContainer'
import { hydrate } from 'react-dom'

const homeContainer = document.getElementById('home-container')


// render(
  hydrate(
  <BrowserRouter>
    <App />
  </BrowserRouter>
  , homeContainer)