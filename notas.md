# Introducción al curso

## __Instalando dependencias__
* Instalamos el proyecto de Platzi Video que hicimos en el curso de Redux
* En el archivo package.json:
  * Quitamos los caracteres ^ para evitar que se actulicen las dependencias 
  * Actualizamos la version de react y react-dom a 16.4.2
  * Instalamos dependencias de [react-router](https://github.com/ReactTraining/react-router)

## __Configurando un Single Page Aplication(SPA) en Webpack__
* Hay dos enfoques al realizar una app con react:
  * Puedes tener un entry point por cada vista(ruta) que tenga la app(Muchos archivos declarados en el objeto entry de los archivos webpack)
  * En una SPA(react-router) solo tienes un entry point, que es el dueño absoluto de todo lo que va a pasar en la app
* En el archivo __webpack.dev.config.js__ y __webpack.config.js__:
  * Al entry point __home__ le cambiamos el nombre a __app__ y cambiamos la direccion para que ahora apunte a __'src/entries/app.js'__
* El componente __entries/home.js__ cambia de nombre, ahora sera __entries/app.js__, para que tenga congruencia con lo explicado en la linea anterior
* En el archivo __index.html__ cambiamos el src del script para que ahora apunte al archivo __http://localhost:9000/js/app.js__

# React Router API 

## BrowserRouter [web](https://reacttraining.com/react-router/web/api/BrowserRouter)
* El componente __< BrowserRouter >__ recibe las siguientes propiedades para ser configurado:
  * __basename__: la ruta con la que arrancaran el componente BrowserRouter (/clases/)
  * __getUserConfirmation__: es una propiedad que recibe una funcion con la cual podemos validar si alguien quiere abandonar una pagina
  * __forceRefresh__: es la propiedad que ayuda a forzar una recarga de pagina cada vez que alguien navegue por esa ruta. Es un valor booleano 
  * __keyLength__: cada vez que hacemos una navegacion se guarda un historial de esta y se le va asignando un id de 6 caracteres. Con la propiedad keylength podemos cambiar ese tamaño
  * __children__: es un componente que esta dentro del BrowserRouter, el wrapper de la app o un componente especifico (en react es igual a {props.children})
* En el componente __entries/app.js__:
  * Importamos __BrowserRouter__ desde react-router-dom
  * En el render envolvemos el contenido del componente __Provider__ dentro del componente __BrowserRouter__. Esto es lo que se denomina __children__ del componente BrowserRouter. Utilizamos la propiedad __basename__ del BrowserRouter

## Tipos de enrutadores
* React-Router es una libreria mas que agregamos a nuestro stack(conjunto de tecnologias que construyen la app)
* __Tipos de enrutadores__: es un componente encargado de manejar el historial del navegador. Los tipos de enrutadores son:
  * __BrowserRouter__: Usa el HTML5 history API. Nos da la posibilidad de cambiar la ruta en la barra de navegacion sin recargar el navegador (https://platzi.com/cursos)
  * __HashRouter__: Funciona igual al BrowserRouter pero anteponiendole un simbolo # al inicio de cada url(https://platzi.com/#/cursos)
  * __MemoryRouter__: Mantiene el historial de busqueda en memoria, perfecto para pruebas sin navegadores. Para pruebas del codigo react
  * __StaticRouter__: Nunca cambia de direccion, perfecto para usar en Server Side Render
  * __NativeRouter__: Para usarr en React Native. Mejor React Navigation
  
## __Link y NavLink__
* El BrowserRouter no sirve de mucho si no tiene enlaces, para esto tenemos dos componentens:
  * Funcionan muy parecido a la etiqueta de ancla __a__ del html
  * __Link__: Tiene las siguientes propiedades
    * __to__: funciona como el href de las etiquetas a. Nos indica a que ruta se dirigira el enlace.
    * __replace__: igual al to pero con una diferencia. Remplaza el contenido de un enlace.
    * __innerRef__: es una forma de obtener el elemento html de ese elemento. Ejemplo obtener el elemento a y asi poder darle estilos.
    * others: son otras propiedades que queramos darle a ese elemento. Debido a que en el fondo es link es una etiqueta a, podemos pasarle todos los atributos de esta etiqueta como propiedades del componente.
  * __NavLink__: Al igual que Link este componente tambien en el fondo es una etiqueta a, solo que tiene mas propiedades para hacerlo un poco mas fancy. Por ejemplo poner una clase a un boton del navegador, cuando estes en una pagina especifica. Tiene las siguientes propiedades:
    * __activeClassName__: sirve para que al estar posicionado en una pagina, en el navbar se active(cambie de color, sombre, etc) el boton de esa pagina.
    * __activeStyle__: es lo mismo que activeClassName pero poniendole los estilos en linea
    * __isActive__: es una funcion que se manda a los elementos del navlink cuando estamos en el foco de esa vista
    * __exact__ y __stric__: rutas exactas y estrictas
    * __location__: sirve para hacer la comparacion de la propiedad isActive con alguna otra ruta
* Creamos el componente __header.js__ y su hoja de estilos __header.css__

* En el archivo __entries/app.js__
  * Importamos el componente __Header__  y __Fragment__
  * En el render utilizamos el componente __Header__ que importamos
  * Debido a que en el render solo podemos pasar un hijo, utilizamos __Fragment__ para envolver a los componentes Header y Home. Lo que hace Fragment es que no se dibuje un div vacio en el html.
  * En el componente __BrowserRouter__ eliminamos la propiedad *basename="/videos"*, ya que no queremos que al seleccionar una seccion del header nos aparezca la ruta *localhost.../videos/contacto*, queremos que aparezca *localhost.../contacto*

* En el componente __header.js__
  * Importamos __Link__ y __NavLink__ desde react-router-dom
  * Inicialmente sustituimos todas las etiquetas __a__ por __link__ y los __href__ por __to__
  * Posteriormente debido que queremos que cuando clickemos en una opcion del navbar esta queda señalizada, con una raya azul en la parte superior, utilizaremos el componente __NavLink__ asi que sustituiremos todos los __Link__
  * Para lograr que la barra azul quede activada cuando pulsemos sobre un boton de navegacion utilizamos la propiedad __activeClassName__
  * Utilizamos la propiedad __exact__ en el NavLink de inicio, esto para que cuando seleccionemos otra opcion del navbar la barra azul superior se elimine de la opcion de Inicio, para que vuelva a visualizase debe ser una ruta exacta
  * *Ojo: NavLink es usado cuando tenemos un navbar y Link se usa para cuando solo queremos llamar una ruta pero sin ponerle alguna señalizacion*

## __Route__
* Para cambiar cosas dentro de la interfaz eso lo hacemos con ruta(Route).
* Podemos especificar a que ruta y que queremos renderizar en esa ruta.
* __Route__ tiene las siguientes propiedades:
  * __component__: que componente quiero renderizar en cierta ruta
  * __path__: sirve para asignar la ruta para renderizar component 
  * __render__: una alternativa de component, para hacer un renderizado en modo de funcion
  * __children__: lo que se encuentra dentro del componente
  * __exact__, __strict__, __sensitive__: nos sirve para especificar que tan riguroso debe ser el match de las rutas, reciben un boleano
  * __exact__: como lo vimos en el modulo anterior(header.js), sirve para que al ingresar a la ruta de una pagina en especial, esta debe ser exacta y asi no implique otras rutas.
  * __strict__: es que sea estricto comn una ruta, por ejemplo si declaramos que la ruta sea /home/ y en el navegador typeamos /home esto marcaria false, ya que no corresponde a lo declarado, pero si typeamos /home/ o /home/one, esto daria true, ya que si cumple con lo declarado, en cambio si fuera exact en el caso de /home/one marcaria un false ya que no es exacto.
  * __sensitive__: Es el Case Sensitive(Mayusculas) de la ruta, si lo pasamos, que seria True, la ruta debera ser escrita tal cual fue declarada, es decir con mayusculas y minusculas, en caso de que no se pase nada se podra acceder a la ruta sin importar si se tipea en mayusculas o minusculas.

* En el componente __entries/app.js__
  * Importamos la dependencia __Route__ desde react-router-dom
  * En el render, cambiamos el componente Home por el componente __Route__, le pasamos las propiedades exact, el path y el componente Home al cual hace referencia
  * Creamos otra ruta(Route) para videos y su componente Videos

* Se crearon componentes Home y la hoja de estilos generic-pages.css
* Se hicieron cambios de nombres en algunos archivos
* En el componente related y en sus estilos se hicieron algunos cambios.

## __Parametros de navegacion__
* Poder personalizar la ruta y pasarle parametros distintos utilizando otras estrategias de navegacion

* En el componente __components/media.js__:
  * Importamos __Link__ desde react-router-dom
  * En el return, envolvemos el contenido(etiqueta div) dentro de la etiqueta __Link__
    * Utilzamo la propiedad __to__ que recibe un objeto con dos keys.
      * __pathname__: que como su nombre lo indica, hace referencia a la ruta donde se visualizara el video
      * __search__: que es un query, que ira cambiando dinamicamente segun el id que le sea apsado
      * __state__: que sirve para pasar el estado, en este caso el modal y el id, en esta app usamos redux el cual ya contiene el estado, por lo cual state sera como ejemplo

* En el componente __components/modal.js__
  * Importamos __Link__ desde react-router-dom
  * En el return agregamos la etiqueta __Link__ y dentro de ella ponemos la etiqueta button
  * A la etiqueta Link le pasamos la prop __to__ que sera igual a un objeto que contendra una llave llamada __pathname__ como se hizo en el componente anterior
  * *Recordamos que dentro de la propiedad to se puede pasar el state , si no tuvieramos redux podriamos usar el state del react-router para manejar el modal, como lo hicimos en este unidad como ejemplo, ya que el state de la app sigue manejandose con redux*

## __Páginas de contenido no encontrado 404__
* Creamos el componente __not-found.js__, el cual renderizara la pagina 404. Usamos PureComponent. Importamos los estilos.
* Dentro del componente __app.js__
  * En el render agregamos otro componente __Route__ el cual contendra el componente __not-found__ importado previamente.

## __Redirect - Switch__
* __Switch__: solo renderiza el primer componente que haga match con la ruta que se esta asignando. Aqui utilizamos switch para que no se renderize el componente NotFound como pasaba antes, ya que como este no tiene ruta definida, se renderiza en todas las paginas que no tengan una ruta asignada y cuando vayamos al home, que la ruta es localhost.../ solo muestre el componente que corresponde a esa ruta, en esta caso el home.
* __Redirect__: Un redirect(redireccionamiento) sirve para tener una url mas corta pero que se redireccione a una url mas grande.

* En el componente __app.js__:
  * Importamos __Switch__ y __Redirect__ desde react-router-dom
  * Utilizamos la etiqueta __Switch__ para envolver las rutas
  * Utilizamos la etiqueta __Redirect__ para hacer un redireccionamiento, cuando la ruta sea /v nos redirija a /videos, para esto usamos las propiedaes from(desde), to(hacia)
  *Debido a que no tenemos una ruta en servidor que responda a /v, asi que haremos un enlace que nos haga el direccionamiento a /v. Veremos el correcto del Redirect cuando veamos SSR(Server Side Rendering)*

* En el componente __header.js__:
  *Agreamos un nuevo NavLink llamado Redirect el cual apuntara hacia la ruta /v, para que se pueda ver el funcionamiento de __Rederict__

## __Prompt, validación antes de dejar la pagina__
* Una validación antes de dejar la pagina es lo que pasa cuando estamos llenando un formulario y nos queremos ir de el sin mandarlo, antes de irnos nos preguntara si realmente queremos abandonar la pagina. En react-router podemos configurar cuando y en donde queremos mandar esas validaciones, esto lo hacemos con __Prompt__.

* En el componente __components/search.js__:
  * Importamos __Prompt__ desde react-router
  * Dentro del form utilizamos el componente __Prompt__ con las propiedades:
    * __when__: es un valor booleano, el cual de ser true disparara el mensaje. Debido a que deseamos saber si el input esta vacio o con valores, le pasamos al __when__ la __props.prompt__, la cual contiene un valor booleano.
    * __message__: mensaje que se mostrara al intentar abandonar la pagina, en forma de alert.

* En el componente __containers/search.js__:
  * Agregamos la llave prompt al state
  * En la funcion __handleInputChange__ seteamos el valor de prompt mediante un not not operator (!!) a la longitud del value *Quedan unos console.log comentados para ver como funciona este operador not not(!!)*
  * En el componente __Search__ pasamos la propiedad prompt y le pasamos el valor de prompt en el state(state.prompt)

## __Manipulando el historial__
* Navegar de una manera mas programatica, cuando ocurra algo en tu pagina, cuando un evento se desencadene, botones mas personalizados.
* Para esto usaremos la propiedad __history__ de react-router con los metodos:
  * __go__: manda a la pagina, segun el numero que sea pasado como parametro. Negativos son paginas atras y positivos hacia adelante
  * __goBack__: regresa a la pagina anterior
  * __goForward__: avanza a la pagina siguiente
  * __push__: agrega una ruta(como el de js)

* En el componente __not-found.js__:
  * Agregamos las arrow functions:
    * __handleBackClick__: que nos retorna la propiedad history con su metodo goBack(comentada) y el metodo go(-1) que retorna a la pagina anterior
    * __handleForwardClick__: nos lleva a la pagina siguiente. Para ver el funcionamiento de este boton, visitamos la seccion contacto y luego la seccion perfil y desde esta __regresamos a la ruta anterior__ con la botonera de la parte inferior de la pagina, este nos regresa a la seccion contacto y aqui podemos utilizar el boton __ir a la ruta siguiente__
    * __handleRandomClick__: utiliza el metodo push para agregar la ruta de un video random. Este metodo lo que hace es que cambia los valores de propiedad location, en especial la que nos interesa __search__. *Esto lo podemos ver con el inspector de react en el navegador*
  * En el return agregamos los respectivos botones para cada arrow function

  * En el componente __containers/video.js__:
    * Utilizamos el ciclo de vida de los componentes __componentDidMount__:
      * Declaramos una constante search que contendra el valor de props.location.search esto es igual a __?id=< No.de video >__
      * Utilizamos una condicional, en el caso de que la constante search contenga un valor, abra el modal y le pasamos como parametro el valor de la constante id, la cual utiliza el metodo substr para obtener el valor del index 4 del string y asi obtener el numero.

## __Obteniendo el historial desde cualquier componente__
* Como pasar las propiedades que da el react-router, como por ejemplo location y history, a otro componente que no sea una pagina, por ejemplo un componente que no sea parte de una ruta. En este caso el Header

* En el componente __header.js__:
  * Importamos la funcion __withRouter__ desde react-router
  * Declaramos la funcion __handleClick__ la cual nos retornara a una ruta atras con el metodo history.goBack
  * Agregamos un nuevo elemento li, el cual sera una etiqueta a que recibe el evento __onClick__ que apunta a la funcion __handleClick__ declarada anteriormente
  * En el export default utilizamos __withRouter__, es parecido a connect de react-redux, para pasar las props.
  *Mediante la consola de React del navegador, podemos observar como la etiqueta Header cambio, ya que ahora se encuentra envuelta por la etiquta withRouter y dentro por otra etiqueta Route, pero al checar sus props ya cuenta con todas las propiedades*

# Server Render

## __Configurando Webpack para server render__
* __Codigo Unviersal__: El codigo de las vistas que va a renderizar desde servidor, va a funcionar tambien en cliente. Para eso funciona el SSR(Server Side Rendering)

* Creamos el archivo __webpack.server.config.js__ el cual contendra la configuracion para el SSR. Esto debido a que vamos a requerir a que unas cosas se exporten por webpack de una manera diferente y seran orientados a que esos archivos no los lea el navegador si no Node
  * Agregamos un nuevo key __target__ el cual tendra de valor __node__. Por default es browser.
  * En el __output__ 
    * Agregamos el key __libraryTarget__ que tendra el valor __commonjs2__ el cual nos sirver para que node pueda cargar modulos.
    * En el key __publicPath__ solo dejamos el valor "/", ya que queremos que sea una ruta relativa de donde nosotros estamos llamando al archivo webpack.server.config.js.
    * En el key __filename__ eliminamos el .[hash] ya que queremos un nombre de archivo mas estatico y que no cambie cada que corramos el archivo. Cambiamos el nombre de la carpeta por __ssr__.
    * En la constante __plugins__, tambien eliminamos el .[hash], por el motivo descrito anteriormente.

* En el archivo __package.json__
  * Agregamos un nuevo build __build:server__ y le pasamos los parametros.

*Corremos el nuevo build:server y nos crea una carpeta ssr con el archivo app.js dentro*

## __Creando una aplicación de Express__
* Instalamos la dependencia __express__(package.json)

* Creamos el archivo __src/server.js__
  * Creamos la const __express__ que contendra el contenido de la dependencia express
  * Cramos const __app__ que inicializa express()
  * Utilizamos el metodo __get__ de la app(app.get) que recibe dos parametros:
    1. La ruta a la cual queremos apuntar. En este caso usamos el comodin * para indicar que sera en todas
    2.  Una funcion que recibe como parametros el __req(request)__ y el __res(response)__ y retorna:
      * Un console.log con la __url(req.url)__
      * El metodo __res.write__ al cual le pasamos como parametro un template string, el cual es el contenido del archivo __index.html__
  * El metodo __res.end()__ para indicar que ya finalizo
  * utilizamos el metodo __app.listen__ y le pasamos como parametro el puerto donde correra la app

  *Debido a que el archivo index.html corre en el navegador y la funcion de SSR es que corra desde el servidor, es por eso que el contenido de index se escribe con el metodo res.write de app. Cuando queramos hacer algo en el navegador, solamente, utilizaremos el otro build el de webpack.dev.config.js*

## __Static Router__
* Los metodos utilizados por BrowerRouter en el navegador(history, location. etc), no se pueden utilizar en el servidor, ya que no existen en NODE. Es por esto que utilizamos el __Static Router__ en el SSR. 
* Para lograr esto necesitamos un archivo de compliacion que sirva para el cliente(Navegador) y otro para el servidor(Node)

* Creamos el componente __containers/appContainer.js__
  * Compiamos todo lo que teniamos en el archivo __entries/app.js__. Debido a que este archivo sera el que este del lado del servidor, eliminamos todo lo que hae referencia a la API de HTML, por que Node no puede interpretarlo, cambiamos algunas rutas de las dependecias y eliminamos el BrowserRouter, que como se comento antes, maneja toda la API de html
  *Este sera el componente que este del lado del servidor SSR* 

* En el componente __entries/app.js__
  * Eliminamos dependecias que ya no usaremos
  * Importamos el componente __App__ desde pages/containers/appContainer.js
  * En el render, dentro del componente BrowserRouter pasamos el componente App
  *Este sera el componente que seguiremos utilizando cuando queramos hacer cambios y verlos en el navegador*

* En el archivo __webpack.server.config.js__
  * En el objeto __entry__ en la llave __app__ cambiamos la ruta del archivo de entrada
  *Se cambio la ruta ya que queremos que apunte al archivo que configuramos para que sea compatible con el servidor(Node) SSR*

* En el archivo __server.js__
  * Importamos __ReactRouter__ desde react-router y la __App__ desde __dist/ssr/app__
  * Agregamos el componente __ReactRouter.StaticRouter__ y dentro pasamos la __App__

*Debido a que en este ultimo archivo necesitamos usar React, es necesario usar Babel ya que el servidor no lo entiende por si solo. Tambien cambiaremos los require por imports. EN LA SIGUIENTE CLASE*

## __ReactDOMServer__
* En este clase complementaremos lo de clase anterior y mejoraremos algunas cosas
* Haremos compatibles Babel y Node para que podamos utilizar react y sus metodos
* Todo esto del lado del servidor(Node)
* Instalamos la dependecia __babel-cli__ el cual nos ayudara a utilizar JS moderno del lado de Node
* Instalamos la dependecia __babel-watch__ para refrescar cuando tengamos cambios
*Debido a que ya instsalamos babel-cli, podemos utilizar los import de ES6*

* En el archivo __server.js__
  * Cambiamos la manera en importar las dependecias, en lugar de require cambiamos a import, gracias a babel-cli. Importamos mas dependecias.
  * Importamos la dependecia [reactDOMServer](https://reactjs.org/docs/react-dom-server.html) la cual mediante el metodo renderToString nos permite utilizar html en nuestro servidor(Node).
  * Utilizamos el metodo __app.use__ para declarar las rutas estaticas. En el primer caso para la carpeta __dist__ y en el segundo caso la carpeta __images__
  * En el metodo app.get 
    * declaramos una constante __html__ 
    * La cual contiene el metodo __reactDOMServer.renderToString()__
      * Recibe como parametro el componente __StaticRouter__ con la propiedad location que sera igual a la __req.url__ para que sea dinamica(__location__ es igual al __to__ de Navlink o Link). Dentro de este componente le pasamos el componente __App__
  *reactDomServer mediante el metodo renderToString, renderiza el componente StaticRouter y su contenido en el servidor, a manera de html. StaticRouter es el componente que usamos en el servidor, ya que como se comento antes el no hereda la API de html por lo cual no causa conflicto con Node*
  * En el metodo res.write
    * Cambiamos la ruta de los archivos css y scripts. Aqui usamos la ruta estatica dist, declarada previamente.
    * En el __div.container__ cambiamos su contendio para que renderize el valor de la constante __html__

* En el archivo __webpack.config.js__
  * Debido a que usamos el script __build:local__ para obtener los archivos locales:
    * Quitamos los .[hash] del archivo css de la constante plugins y del filename.
    * Refactorizamos la ruta a donde debe apuntar el publicPath

* En el archivo __package.json__
  * Creamos dos nuevos scripts
    1. __server__: el cual contiene __babel_node__ la ruta del archivo que pasara por babel y los presets para que puede usar react y lo mas nuevo de EmacScript *gracias a esto es que en server.js podemos usar imports*
    2. __watch:server__: funciona como server la diferencia es que reiniciara el servidor cada vez que registre un cambio en el codigo.

## __Creando una página unica por video__
* Crearemos una pagina a la cual llegaremos por server(visualizada en el navegador) para ver los videos.

* Creamos el componente __containers/video.js__:
  * Importamos las dependecias
  * Creamos la clase __Video__ la cual hara render: 
    * Si existe la propiedad __existId__ entonceshara un return del componente __VideoPlayer__ con el __id__ como propiedad. 
    * Si no existe el id retornara el componente __NotFound__
  * Creamos la funcion __mapStateToProps__ que recibe los parametros state, props y dentro:
    * Creamos la constante __id__ que sera igual al id que existe dentro de las propiedades del componente __props.match.params.id__(lo podemos visualizar mediante la consola de react)
    * Returnamos un objeto con las llaves:
      * __existId__: tiene como valor la comprobacion de que en el state existe el media con ese id __.get('media).has(id)__
      * __id__: que sera el valor de la constante __id__
  * En el export default hacemos el __connect__ de __mapStateToProps__ con el componente __Video__, es decir con el componente de este archivo.

  * En el componente __appContainer.js__:
    * Importamos el componente __Video__
    * Creamos un componente __Route__ y le pasamos las propiedades:
      * __path__: donde la ruta incluira el id, esto con la sintaxis __:id__ 
      * __componente__: le pasamos el componente __Video__ previamente importado
    * Creamos un nuevo __redirect__ solo para ver que los parametros pueden ser mas complejos, en este caso podemos redireccionar la ruta __/v/:id__ a __/videos/:id__

*Para que los cambios se vean reflejados en nuestro server SSR: Primero corremos el script build:local esto para que se creen los archivos dist/js/app.js y sus dist/css/css estos son consumidos por el archivo server.js, en los scripts dentro de la estructura de un archivo html. Segundo en el script build:server nos crea los archivos dist/ssr/app.js, la cual es consumida en el archivo server.js, ya que es la ruta del componente App*

* Cuando estamos en la pagina __NotFound 404__ y presionamos el boton de __Video Random__ no pasa nada, pero en la devtools de chrome si vemos un error 'push of undefined', eso pasa por que NotFound no esta recibiendo las props de history. Entones le vamos a pasar estas props directamente al archivo __not-found.js__
* En el componente __not-found.js__:
  * Importamos __withRouter__, el cual sirve para pasar las props a un componente
  * En el export default agregamos withRouter para pasar las props a este componente, NotFound.

* En el componente __entries/app.js__
  * Importamos __hydrate__ de react-dom
  * Cambiamos el __render__ por __hydrate__
  * En el caso del metodo __render__ es recomendado si se hace un renderizado en el cliente(browser), pero si se requiere un renderizado en el servidor se recomienda usar __hydrate__
*render lo que hace es que renderiza toda la app, mientras que hydrate enlaza los eventos y los une a lo que ya construyo en el server. Un render completo es pesado y hydrate no tanto*